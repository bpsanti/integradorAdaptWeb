/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package handlers;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OntologyRoutine {
    OntModel schema = ModelFactory.createOntologyModel();

    public void updateOntology(MySQL database, TableHandler tableHandler) throws FileNotFoundException, SQLException {
        OntologyHandler ontologyHandler = new OntologyHandler();
        StringHandler stringHandler = new StringHandler();

        ResultSet result = tableHandler.select(database, "Usuarios");
        while (result.next()) {
            String tipoUsuario = (result.getString(6));

            // dependendo do tipo de usuario, existe um update na ontologia diferente
            switch (tipoUsuario) {
                case "aluno":
                    ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1322071577.owl#");
                    Individual student = ontologyHandler.createInstance("Student", "STUDENT_" + result.getString(1));
                    ontologyHandler.addDataProperty(student, "studentName", stringHandler.returnString(result.getString(2)));
                    ontologyHandler.addDataProperty(student, "studentID", result.getInt(1));
                    break;
                case "professor":
                    ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1315497000.owl#");
                    Individual contributor = ontologyHandler.createInstance("Contributor", "CONTRIBUTOR_" + result.getString(1));
                    //contentOntology.addDataProperty(contributor, "cName", stringHandler.returnString(result.getString(2)));
                    //contentOntology.addDataProperty(contributor, "cID", result.getInt(1));
                    break;
            }
        }


        result = tableHandler.select(database, "Dispositivos");
        while (result.next()) {
            int codigoUsuario         = result.getInt(2);
            String tipoDispositivo    = result.getString(4);
            String nomeDispositivo    = "DEVICE_" + codigoUsuario;
            String conexaoDispositivo = result.getString(8);

            Individual instance = null;

            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1306263985.owl#");
            switch (tipoDispositivo) {
                case "Movel":
                    instance = ontologyHandler.createInstance("SmartPhone", nomeDispositivo);
                    break;
                case "Desktop":
                    instance = ontologyHandler.createInstance("FixedDevice", nomeDispositivo);
                    break;
            }

            switch (conexaoDispositivo) {
                case "rapida":
                    ontologyHandler.addExternalObjectProperty(instance, "hasNetConnection", "Good");
                    break;
                default:
                    ontologyHandler.addExternalObjectProperty(instance, "hasNetConnection", "Bad");
                    break;
            }


            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1322071577.owl#");
            Individual student = ontologyHandler.getInstance("STUDENT_" + codigoUsuario);

            if (student != null)
                ontologyHandler.addObjectProperty(student, "isUsing", nomeDispositivo);
        }

        result = tableHandler.select(database, "Curso");
        while (result.next()) {
            int codigoCurso = result.getInt(1);
            String nomeCurso = (result.getString(2));
            int codigoUsuario = result.getInt(3);

            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1315497000.owl#");
            Individual instance = ontologyHandler.createInstance("Course", "C_" + codigoCurso);

            ontologyHandler.addDataProperty(instance, "loName", nomeCurso);
            ontologyHandler.addDataProperty(instance, "loID", codigoCurso);
            ontologyHandler.addDataProperty(instance, "learningResourceType", "Course");
            ontologyHandler.addDataProperty(instance, "language", "Portuguese");
        }

        result = tableHandler.select(database, "Disciplinas");
        while (result.next()) {
            int codigoDisciplina = result.getInt(1);
            String nomeDisciplina = (result.getString(3));

            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1315497000.owl#");
            Individual instance = ontologyHandler.createInstance("Discipline", "D_" + codigoDisciplina);

            ontologyHandler.addDataProperty(instance, "loName", nomeDisciplina);
            ontologyHandler.addDataProperty(instance, "loID", codigoDisciplina);
            ontologyHandler.addDataProperty(instance, "learningResourceType", "Discipline");
            ontologyHandler.addDataProperty(instance, "language", "Portuguese");
        }

        result = tableHandler.select(database, "CursoDisciplina");
        while (result.next()) {
            Individual instance = null;
            int codigoCurso = result.getInt(1);
            int codigoDisciplina = result.getInt(2);

            String nomeCurso = "C_" + codigoCurso;
            String nomeDisciplina = "D_" + codigoDisciplina;

            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1315497000.owl#");
            //ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1322071577.owl#");
            instance  = ontologyHandler.getInstance(nomeCurso);
            Individual instance2 = ontologyHandler.getInstance(nomeDisciplina);

            if (instance != null && instance2 != null) {
                ontologyHandler.addExternalObjectProperty(instance, "customizes", nomeDisciplina);
                ontologyHandler.addExternalObjectProperty(instance2, "isCustomizedFor", nomeCurso);
            }
        }

        result = tableHandler.select(database, "Keyword");
        while (result.next()) {
            int codigoKeyword = result.getInt(1);
            String nomeKeyword = "KEY_" + codigoKeyword;
            String descKeyword = result.getString(2);

            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1315497000.owl#");
            Individual instance = ontologyHandler.createInstance("Keyword", nomeKeyword);

            ontologyHandler.addDataProperty(instance, "name", descKeyword);
        }

        result = tableHandler.select(database, "Topicos");
        while (result.next()) {
            int codigoTopico = result.getInt(1);
            int codigoDisciplina = result.getInt(3);
            String numTopico = result.getString(4);
            String descTopico = (result.getString(5));
            String descKeyword = result.getString(8);
            //int codigoDisciplina = result.getInt(2);

            String nomeTopico = "T_" + codigoDisciplina + "_" + numTopico;
            String nomeDisciplina = "D_" + codigoDisciplina;

            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1315497000.owl#");
            Individual instance = ontologyHandler.createInstance("Topic", nomeTopico);

            ontologyHandler.addExternalObjectProperty(instance, "isPartOf", nomeDisciplina);
            ontologyHandler.addDataProperty(instance, "loName", numTopico);
            ontologyHandler.addDataProperty(instance, "loID", codigoTopico);
            ontologyHandler.addDataProperty(instance, "language", "Portuguese");
            ontologyHandler.addDataProperty(instance, "description", descTopico);
            ontologyHandler.addDataProperty(instance, "learningResourceType", "Concept");


            int keywordID = tableHandler.getKeywordID(database, descKeyword);
            ontologyHandler.addExternalObjectProperty(instance, "hasKeyword", "KEY_" + keywordID);

            // adiciona a propriedade inversa p disciplina
            instance = ontologyHandler.createInstance("Discipline", nomeDisciplina);

            ontologyHandler.addExternalObjectProperty(instance, "hasPart", nomeTopico);
        }

        result = tableHandler.select(database, "TopicoRequisito");
        while (result.next()) {
            int    codigoTopico  = result.getInt(2);
            String numRequisito  = result.getString(3);

            ResultSet result2    = tableHandler.getTopic(database, codigoTopico);
            result2.next();

            int    codigoDisc    = result2.getInt(3);
            String numTopico     = result2.getString(4);
            String nomeTopico    = "T_" + codigoDisc + "_" + numTopico;
            String nomeRequisito = "T_" + codigoDisc + "_" + numRequisito;

            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1315497000.owl#");

            Individual instance  = ontologyHandler.getInstance(nomeRequisito);
            ontologyHandler.addExternalObjectProperty(instance, "learningPath", nomeTopico);

            instance  = ontologyHandler.getInstance(nomeTopico);
            ontologyHandler.addExternalObjectProperty(instance, "prevLearningPath", nomeRequisito);
        }

        result = tableHandler.select(database, "LearningObjects");
        while (result.next()) {
            int codigoLO = result.getInt(1);
            int codigoDisc = result.getInt(3);
            String numTopico = result.getString(10);
            String nomeTopico = "T_" + codigoDisc + "_" + numTopico;
            String nomeLO = (result.getString(4));
            String descLO = (result.getString(5));
            String tipoLO = null;
            String nome = null;
            String dificuldade = null;
            String descKeyword = result.getString(6);

            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1315497000.owl#");
            Individual instance = null;

            switch (result.getInt(8)) {
                case 0:
                    nome = "CM_" + codigoLO;
                    instance = ontologyHandler.createInstance("Study", nome);
                    tipoLO = "ComplementaryMaterial";
                    break;
                case 1:
                    nome = "ST_" + codigoLO;
                    tipoLO = "Example";
                    switch (result.getInt(9)) {
                        case 1:
                            dificuldade = "Easy";
                            break;
                        case 2:
                            dificuldade = "Medium";
                            break;
                        case 3:
                            dificuldade = "Complex";
                            break;
                    }

                    instance = ontologyHandler.createInstance("Study", nome);
                    break;
                case 2:
                    nome = "EX_" + codigoLO;
                    tipoLO = "Exercise";
                    switch (result.getInt(9)) {
                        case 1:
                            dificuldade = "Easy";
                            break;
                        case 2:
                            dificuldade = "Medium";
                            break;
                        case 3:
                            dificuldade = "Complex";
                            break;
                    }
                    instance = ontologyHandler.createInstance("Exercise", nome);
                    break;
            }

            ontologyHandler.addExternalObjectProperty(instance, "supportedBy", nomeTopico);
            ontologyHandler.addDataProperty(instance, "loName", nomeLO);
            ontologyHandler.addDataProperty(instance, "loID", codigoLO);
            ontologyHandler.addDataProperty(instance, "language", "Portuguese");
            ontologyHandler.addDataProperty(instance, "activityName", descLO);
            ontologyHandler.addDataProperty(instance, "learningResourceType", tipoLO);

            int keywordID = tableHandler.getKeywordID(database, descKeyword);
            ontologyHandler.addExternalObjectProperty(instance, "hasKeyword", "KEY_" + keywordID);

            if (dificuldade != null)
                ontologyHandler.addDataProperty(instance, "difficulty", dificuldade);

            instance = ontologyHandler.getInstance(nomeTopico);
            ontologyHandler.addExternalObjectProperty(instance, "supports", nome);
        }

        result = tableHandler.select(database, "LogUsuario");
        while(result.next()) {
            int codigoUsuario       = result.getInt(2);
            int codigoCurso         = result.getInt(3);
            int codigoDisciplina    = result.getInt(4);
            String numeroTopico     = result.getString(5);
            String tipoLogUsuario   = result.getString(6);
            String estiloLogUsuario = result.getString(7);
            int tipoLO = 0;
            String tipoLO2 = null;

            switch (tipoLogUsuario) {
                case "exemplo":
                    tipoLO = 1;
                    tipoLO2 = "ST_";
                    break;
                case "exercicio":
                    tipoLO = 2;
                    tipoLO2 = "EX_";
                    break;
                case "material":
                    tipoLO = 0;
                    tipoLO2 = "CM_";
                    break;
            }

            ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1322071577.owl#");
            Individual instance = ontologyHandler.getInstance("STUDENT_" + codigoUsuario);

            if (tipoLogUsuario.equals("exemplo") || tipoLogUsuario.equals("exercicio") || tipoLogUsuario.equals("material")) {
                ResultSet result2 = tableHandler.getLearningObjects(database, codigoDisciplina, numeroTopico, tipoLO);

                while(result2.next()) {
                    ontologyHandler.addObjectProperty(instance, "isDoing", tipoLO2 + result2.getInt(1));
                }
            } else {
                ontologyHandler.addObjectProperty(instance, "access", "T_" + codigoDisciplina + "_" + numeroTopico);
            }
        }


        result = tableHandler.select(database, "Matricula");
        while(result.next()) {
            int codigoUsuario       = result.getInt(2);
            int codigoCurso         = result.getInt(3);

            //ontologyHandler.setSOURCE("http://www.owl-ontologies.com/Ontology1322071577.owl#");
            Individual instance = ontologyHandler.getInstance("STUDENT_" + codigoUsuario);

            if (instance != null)
                ontologyHandler.addObjectProperty(instance, "hasLearningGoal", "C_" + codigoCurso);

        }
        //ontologyHandler.listProperties();

        ontologyHandler.writeToOntology();
    }
}
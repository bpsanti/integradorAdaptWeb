package handlers;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDF;
import objects.Object;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OntologyHandler {
    OntModel schema = ModelFactory.createOntologyModel();
    String   SOURCE;
    String   SOURCEFILE = "http://www.owl-ontologies.com/Ontology1322071577.owl#"; //

    public OntologyHandler() {
        schema.read("file:///D:/Ontologias//Onto_Student.owl");
    }

    public Individual createInstance(String className, String instanceName) {
        OntClass instClass  = schema.getOntClass(SOURCE + className);
        //listClasses();
       // System.out.println(SOURCE + className);
        //System.out.println("Classe: " + instClass.toString());

        Individual instance = instClass.createIndividual(SOURCEFILE + instanceName);

        return instance;
    }

    public Individual getInstance(String instanceName) {
        Individual instance = schema.getIndividual(SOURCEFILE + instanceName);

        return instance;
    }

    public void addObjectProperty(Individual instance, String propertyName, String property) {
        Resource resource       = schema.getResource(instance.toString());
        Property objectProperty = schema.getObjectProperty(SOURCEFILE + propertyName);
        Resource propertyValue  = schema.getResource(SOURCEFILE + property);

        schema.add(resource, objectProperty, propertyValue);
    }

    public void addExternalObjectProperty(Individual instance, String propertyName, String property) {
        Resource resource       = schema.getResource(instance.toString());
        Property objectProperty = schema.getObjectProperty(SOURCE + propertyName);
        Resource propertyValue  = schema.getResource(SOURCEFILE + property);

        schema.add(resource, objectProperty, propertyValue);
    }

    public void addDataProperty(Individual instance, String propertyName, String property) {
        Resource resource       = schema.getResource(instance.toString());
        Property dataProperty   = schema.getProperty(SOURCE + propertyName);

        schema.add(resource, dataProperty, property);
    }

    public void addDataProperty(Individual instance, String propertyName, int property) {
        Resource resource       = schema.getResource(instance.toString());
        Property dataProperty   = schema.getProperty(SOURCE + propertyName);

        schema.addLiteral(resource, dataProperty, property);
    }

    public void listClasses() {
        ExtendedIterator classes = schema.listClasses();
        //ExtendedIterator classes = schema.listObjectProperties();

        while (classes.hasNext()) {
            OntClass thisClass = (OntClass) classes.next();
            //ObjectProperty thisClass = (ObjectProperty) classes.next();
            System.out.println("Found class: " + thisClass.toString());
        }
    }

    public void listProperties() {
        ExtendedIterator classes = schema.listObjectProperties();

        while (classes.hasNext()) {
            ObjectProperty thisClass = (ObjectProperty) classes.next();
            System.out.println("Found class: " + thisClass.toString());
        }
    }
    public void setSOURCE(String SOURCE) {
        this.SOURCE = SOURCE;
    }


    public void writeToOntology() throws FileNotFoundException {
        FileOutputStream foutTopicos = new FileOutputStream("D:/Ontologias/Onto_Student2.owl");
        schema.write(foutTopicos);
        schema.close();
        System.out.println("fim");
    }
}

package handlers;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import integrador.Parser;
import objects.*;
import objects.Object;
import org.jdom2.JDOMException;

import javax.xml.transform.Result;

public class TableHandler {
    private ResultSet result;
    private ResultSet resultAux;
    //private String table;
    
    public boolean isTableEmpty(MySQL database, String table) throws SQLException {
        try {
            this.result = null;

            database.setQuery("SELECT * FROM " + table);
            this.result = database.executeQuery();
            
            if(!this.result.first()) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("SQL Error: "+e.getMessage());
        }
        
        return false;
    }
    
    public Timestamp getTableUpdate(MySQL database, String table) throws SQLException {
        Timestamp updateTime = null;
        
        try {
            database.setQuery("SELECT UPDATE_TIME "
                                + "FROM   information_schema.tables "
                                + "WHERE  TABLE_SCHEMA = '"+database.getDatabase()+"' "
                                + "AND TABLE_NAME = '"+table+"'");

            this.result = database.executeQuery();
            this.result.next();
            
            updateTime = this.result.getTimestamp(1);           
        } catch (SQLException e) {
            System.out.println("Erro na execucao da query!");
        }
        
        return updateTime;
    }
    
    public void update(MySQL dbAdaptWebOld, MySQL dbAdaptWebNew, String oldTable, String newTable) throws SQLException {
        Object object = null;
        Timestamp dbOldUpdateTime = getTableUpdate(dbAdaptWebOld, oldTable);
        Timestamp dbNewUpdateTime = getTableUpdate(dbAdaptWebNew, newTable);
        
        switch (oldTable) {
            case "Usuario":
                object = new Usuario();
                break;
            case "Disciplina":
                object = new Disciplina();
                break;
            case "Tecnologia":
                object = new Dispositivo();
                break;
            case "Curso":
                object = new Curso();
                break;
            case "Curso_Disc":
                object = new CursoDisciplina();
                break;
            case "Keyword":
                object = new Keyword();
                break;
            case "Log_Usuario":
                object = new LogUsuario();
                break;
            case "Matricula":
                object = new Matricula();
                break;
        }
        
        if (isTableEmpty(dbAdaptWebNew, newTable)) {
            insertAll(dbAdaptWebOld, dbAdaptWebNew, oldTable, newTable, object);
        } else if (dbOldUpdateTime.after(dbNewUpdateTime)) {

            dbAdaptWebOld.setQuery("SELECT * FROM " + oldTable);
            dbAdaptWebNew.setQuery("SELECT * FROM " + newTable);

            this.result = dbAdaptWebOld.executeQuery();
            this.resultAux = dbAdaptWebNew.executeQuery();

            // Implementar o resto depois.
        }       
    }
    
    public void updateHybrid(MySQL dbAdaptWebNew, String tableOrigin, String tableDestination) throws SQLException, JDOMException, IOException {
        ArrayList<String> queryList = new ArrayList<>();
        Parser parser;
        
        if (isTableEmpty(dbAdaptWebNew, tableDestination)) {
            switch(tableDestination) {
                case "Topicos":
                    dbAdaptWebNew.setQuery("SELECT `CodigoUsuario`, `CodigoDisciplina` FROM " + tableOrigin + " WHERE `StatusDisciplina` ORDER BY `CodigoDisciplina`");

                    this.result = dbAdaptWebNew.executeQuery();

                    int topicsID = 1;
                    while (this.result.next()) {
                        parser = new Parser(this.result.getInt(1), this.result.getInt(2), topicsID);
                        queryList.addAll(parser.startTopicParser(this.result.getInt(1), this.result.getInt(2)));
                        topicsID = parser.getTopicoID();
                    }

                    for (String query : queryList) {
                        dbAdaptWebNew.setQuery(query);
                        dbAdaptWebNew.updateQuery();
                    }
                    break;
                case "LearningObjects":
                    dbAdaptWebNew.setQuery("SELECT `CodigoUsuario`, `CodigoTopico`, `CodigoDisciplina`, `ArquivoTopico`, `NumeroTopico` FROM " + tableOrigin + " WHERE `StatusExemploTopico` OR `StatusExercicioTopico` OR `StatusMaterialTopico` GROUP BY `ArquivoTopico` ORDER BY `CodigoTopico`");

                    this.result = dbAdaptWebNew.executeQuery();
                    this.result.next();
                    while (this.result.next()) {
                        parser = new Parser(this.result.getInt(1), this.result.getInt(3), this.result.getString(4), this.result.getString(5));
                        queryList.addAll(parser.startLearningObjectParser(this.result.getInt(1), this.result.getInt(2), this.result.getInt(3)));
                    }

                    for (String query : queryList) {
                        dbAdaptWebNew.setQuery(query);
                        dbAdaptWebNew.updateQuery();
                    }
                    break;
            }
        }
    }
    
    public void insertAll(MySQL dbAdaptWebOld, MySQL dbAdaptWebNew, String oldTable, String newTable, Object object) throws SQLException {
        String query;
        
        switch (oldTable) {
            case "Tecnologia":
                dbAdaptWebOld.setQuery("SELECT * FROM "
                                        + "(SELECT * FROM `tecnologia` ORDER BY `data` DESC) as temp_table "
                                        + "GROUP BY `id_usuario`");
                this.result = dbAdaptWebOld.executeQuery();
                break;
            case "Keyword":
                dbAdaptWebNew.setQuery("SELECT keywordlearningobject FROM learningobjects UNION " +
                        "SELECT keywordtopico FROM topicos ORDER BY keywordlearningobject ASC");
                this.result = dbAdaptWebNew.executeQuery();
                break;
            case "Log_Usuario":
                dbAdaptWebOld.setQuery("SELECT * FROM `log_usuario` WHERE `data_acesso` >= DATE(NOW()) - INTERVAL 7 DAY");
                this.result = dbAdaptWebOld.executeQuery();
                break;
            case "Matricula":
                dbAdaptWebOld.setQuery("SELECT DISTINCT id_usuario, id_curso FROM `matricula` WHERE `id_professor` > 0 ");
                this.result = dbAdaptWebOld.executeQuery();
                break;
            default:
                dbAdaptWebOld.setQuery("SELECT * FROM " + oldTable);
                this.result = dbAdaptWebOld.executeQuery();
                break;
        }


        while(this.result.next()) {
            query = object.prepareInsertQuery(this.result);

            dbAdaptWebNew.setQuery(query);
            dbAdaptWebNew.updateQuery();
        }
    }
    
    public ResultSet select(MySQL database, String table) throws SQLException {
        String query = "SELECT * FROM "+table;

        database.setQuery(query);
        this.result = database.executeQuery();
        
        return this.result;
    }

    public String getUserName(MySQL database, String column, int userID) throws SQLException {
        String query = "SELECT " +column+ " FROM Usuarios WHERE CodigoUsuario = " + userID;
        
        database.setQuery(query);
        this.result = database.executeQuery();
        this.result.next();
        
        return this.result.getString(1);
    }

    public ResultSet getTopic(MySQL database, int topicID) throws SQLException {
        String query = "SELECT * FROM Topicos WHERE CodigoTopico = " + topicID;

        database.setQuery(query);
        this.result = database.executeQuery();

        return this.result;
    }

    public int getKeywordID(MySQL database, String descKeyword) throws SQLException {
        String query = "SELECT CodigoKeyword FROM Keyword WHERE NomeKeyword = '" + descKeyword + "'";

        database.setQuery(query);
        this.result = database.executeQuery();
        this.result.next();

        return this.result.getInt(1);
    }

    public ResultSet getLearningObjects(MySQL database, int codigoDisciplina, String numeroTopico, int tipoLO) throws SQLException {
        String query = "SELECT * FROM LearningObjects WHERE CodigoDisciplina = '" + codigoDisciplina + "' " +
                "AND NumeroTopico = '" + numeroTopico + "' AND TipoLearningObject = '" + tipoLO + "'";

        database.setQuery(query);
        this.result = database.executeQuery();

        return this.result;
    }

    public String getCourseName(MySQL database, String column, int courseID) throws SQLException {
        String query = "SELECT " +column+ " FROM Curso WHERE CodigoCurso = " + courseID;

        database.setQuery(query);
        this.result = database.executeQuery();
        this.result.next();

        return this.result.getString(1);
    }

    public String getDisciplineName(MySQL database, String column, int courseID) throws SQLException {
        String query = "SELECT " +column+ " FROM Disciplinas WHERE CodigoDisciplina = " + courseID;

        database.setQuery(query);
        this.result = database.executeQuery();


        while(this.result.next())
            return this.result.getString(1);

        return null;
    }
    
    public void deleteAll(MySQL database, String table) throws SQLException {
        String query = "DELETE FROM "+table+" WHERE 1";
        database.setQuery(query);
        database.updateQuery();
        
        query = "TRUNCATE TABLE " +table;
        database.setQuery(query);
        database.updateQuery();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package handlers;

import java.util.ArrayList;
import java.util.Collections;

public class StringHandler {
    public String returnString(String keyword) {
        if (keyword == null)
            return null;


        int tamString = keyword.length();

        String strArray[] = keyword.split("");

        for (int i = 0; i < tamString; i++) {
            //parts[0].equals("numtop")
            switch (strArray[i]) {
                case " ":
                    strArray[i] = "-";
                    break;
                case ",":
                    strArray[i] = "";
                    break;
                case "/":
                    strArray[i] = "-";
                    break;

                case ".":
                    strArray[i] = "";
                    break;

                case "\\":
                    strArray[i] = "-";
                    break;

            }

        }

        StringBuilder keywordNova = new StringBuilder();

        for (int i = 0; i < tamString; i++) {
            keywordNova.append(strArray[i]);
        }

        String keywordFinal = keywordNova.toString();

        return keywordFinal;

    }
}

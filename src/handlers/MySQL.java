package handlers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQL { 
    private Connection connection = null;
    private String query = null;
    private ResultSet result = null;
    private String host = null;
    private String user = null;
    private String pass = null;
    private String database = null;
    private int intReturn;
    
    public MySQL(String host, String database, String user, String pass) {
        this.host = host;
        this.database = database;
        this.user = user;
        this.pass = pass;
    }
    
    public Connection getConnection() throws Exception {
        loadDriver();
        
        try {
            // Host, User, Password
            this.connection = DriverManager.getConnection("jdbc:mysql://"+this.host+this.database, this.user, this.pass);            
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            System.out.println("Não foi possivel conectar ao banco de dados!");
        }
        
        return this.connection;
    }
    
    public void endConnection() throws Exception {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("Não foi possivel encerrar a conexão ao banco de dados!");
        }
    }
            
    public void statusConnection() {
        if (this.connection != null) {
            System.out.println("Conexão: OK.");
        } else {
            System.out.println("Conexão: ERRO.");
        }
    }
    
    public void loadDriver() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            System.out.println("Erro: Erro ao carregar o driver MySQL!");
        }
    }

    public ResultSet executeQuery() throws SQLException {
        try {
            Statement statement = connection.createStatement();
            this.result = statement.executeQuery(this.query);
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("Erro ao executar a query!");
        }
        
        return this.result;
    }
    
    public int updateQuery() throws SQLException {
        try {
            Statement statement = connection.createStatement();
            this.intReturn = statement.executeUpdate(this.query);
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("Erro ao executar a query!");
        }
        
        return this.intReturn;
    }
    
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public ResultSet getResult() {
        return result;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }
}
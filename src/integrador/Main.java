package integrador;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import static javafx.application.Application.launch;

public class Main extends Application {

    Controller myController;

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("gui.fxml"));
        Parent root = (Parent) fxmlLoader.load();
        primaryStage.setTitle("Integrador - AdaptWeb");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();

        myController = fxmlLoader.getController();

    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
    /*
    public static void main(String[] args) throws Exception {
        // treta do Jena
        org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);

        // Conexoes ativas no DB Mysql.
        MySQL dbAdaptWebOld = new MySQL("localhost:3306/", "AdaptWebClean", "root", "vertrigo");
        MySQL dbAdaptWebNew = new MySQL("localhost:3306/", "AdaptWebNew", "root", "vertrigo");
        // Fim das conexoes.
        
        TableHandler tableHandler = new TableHandler(); // Utilizado para manipular o banco.
        OntologyHandler ontologyHandler = new OntologyHandler(); // Manipula as Ontologias.
     
        dbAdaptWebOld.getConnection();
        dbAdaptWebNew.getConnection();
        
        //tableHandler.updateHybrid(dbAdaptWebNew, "Disciplinas", "Topicos");
        //tableHandler.updateHybrid(dbAdaptWebNew, "Topicos", "LearningObjects");
        //tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Disciplina", "Disciplinas");
        //tableHandler.deleteAll(dbAdaptWebNew, "Topicos");
            
        ontologyHandler.updateOntology(dbAdaptWebNew, tableHandler);

        dbAdaptWebOld.endConnection();
        dbAdaptWebNew.endConnection();
    }*/
    
}

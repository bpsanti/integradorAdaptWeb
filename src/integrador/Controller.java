package integrador;

import handlers.MySQL;
import handlers.OntologyHandler;
import handlers.OntologyRoutine;
import handlers.TableHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.io.*;

/**
 * Created by santi on 15/06/2016.
 */
public class Controller {

    @FXML
    private TextField hostAdaptWeb;

    @FXML
    private TextField databaseNameAdaptWeb;

    @FXML
    private TextField usernameAdaptWeb;

    @FXML
    private TextField passwordAdaptWeb;

    @FXML
    private TextField hostNewDatabase;

    @FXML
    private TextField databaseNameNewDatabase;

    @FXML
    private TextField usernameNewDatabase;

    @FXML
    private TextField passwordNewDatabase;

    @FXML
    private TextField pathTextField;

    @FXML
    private void initialize() throws IOException, ClassNotFoundException {
        File file = new File("config.dat");
        Boolean listaVazia = true;
        if(!file.exists()) {
            new File("config.dat").createNewFile();
        } else {
            FileInputStream istream = new FileInputStream("config.dat");
            ObjectInputStream in = new ObjectInputStream(istream);
            while (istream.available() > 0) {
                hostAdaptWeb.setText(in.readObject().toString());
                databaseNameAdaptWeb.setText(in.readObject().toString());
                usernameAdaptWeb.setText(in.readObject().toString());
                passwordAdaptWeb.setText(in.readObject().toString());

                hostNewDatabase.setText(in.readObject().toString());
                databaseNameNewDatabase.setText(in.readObject().toString());
                usernameNewDatabase.setText(in.readObject().toString());
                passwordNewDatabase.setText(in.readObject().toString());

                pathTextField.setText(in.readObject().toString());
            }

            in.close();

        }
    }

    @FXML
    private void updateOntology() throws Exception {
        // Conexoes ativas no DB Mysql.
        MySQL dbAdaptWebOld = new MySQL(hostAdaptWeb.getText(), databaseNameAdaptWeb.getText(), usernameAdaptWeb.getText(), passwordAdaptWeb.getText());
        MySQL dbAdaptWebNew = new MySQL(hostNewDatabase.getText(), databaseNameNewDatabase.getText(), usernameNewDatabase.getText(), passwordNewDatabase.getText());

        TableHandler tableHandler = new TableHandler(); // Utilizado para manipular o banco.
        OntologyRoutine ontologyRoutine = new OntologyRoutine(); // Manipula as Ontologias.

        dbAdaptWebOld.getConnection();
        dbAdaptWebNew.getConnection();

        ontologyRoutine.updateOntology(dbAdaptWebNew, tableHandler);

        dbAdaptWebOld.endConnection();
        dbAdaptWebNew.endConnection();
    }

    @FXML
    private void updateDatabase() throws Exception {
        // Conexoes ativas no DB Mysql.
        MySQL dbAdaptWebOld = new MySQL(hostAdaptWeb.getText(), databaseNameAdaptWeb.getText(), usernameAdaptWeb.getText(), passwordAdaptWeb.getText());
        MySQL dbAdaptWebNew = new MySQL(hostNewDatabase.getText(), databaseNameNewDatabase.getText(), usernameNewDatabase.getText(), passwordNewDatabase.getText());

        TableHandler tableHandler = new TableHandler(); // Utilizado para manipular o banco.

        dbAdaptWebOld.getConnection();
        dbAdaptWebNew.getConnection();

        tableHandler.deleteAll(dbAdaptWebNew, "Usuarios");
        tableHandler.deleteAll(dbAdaptWebNew, "Dispositivos");
        tableHandler.deleteAll(dbAdaptWebNew, "Disciplinas");
        tableHandler.deleteAll(dbAdaptWebNew, "Curso");
        tableHandler.deleteAll(dbAdaptWebNew, "CursoDisciplina");
        tableHandler.deleteAll(dbAdaptWebNew, "Topicos");
        tableHandler.deleteAll(dbAdaptWebNew, "LearningObjects");
        tableHandler.deleteAll(dbAdaptWebNew, "TopicoCurso");
        tableHandler.deleteAll(dbAdaptWebNew, "TopicoRequisito");
        tableHandler.deleteAll(dbAdaptWebNew, "Keyword");
        tableHandler.deleteAll(dbAdaptWebNew, "LogUsuario");
        tableHandler.deleteAll(dbAdaptWebNew, "Matricula");

        tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Usuario", "Usuarios");
        tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Tecnologia", "Dispositivos");
        tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Disciplina", "Disciplinas");
        tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Curso", "Curso");
        tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Curso_Disc", "CursoDisciplina");
        tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Log_Usuario", "LogUsuario");
        tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Matricula", "Matricula");
        tableHandler.updateHybrid(dbAdaptWebNew, "Disciplinas", "Topicos");
        tableHandler.updateHybrid(dbAdaptWebNew, "Topicos", "LearningObjects");
        tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Keyword", "Keyword");

        //tableHandler.update(dbAdaptWebOld, dbAdaptWebNew, "Disciplina", "Disciplinas");

        dbAdaptWebOld.endConnection();
        dbAdaptWebNew.endConnection();
    }

    @FXML
    private void saveConfig() throws IOException {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("config.dat"));

            out.writeObject(hostAdaptWeb.getText());
            out.writeObject(databaseNameAdaptWeb.getText());
            out.writeObject(usernameAdaptWeb.getText());
            out.writeObject(passwordAdaptWeb.getText());

            out.writeObject(hostNewDatabase.getText());
            out.writeObject(databaseNameNewDatabase.getText());
            out.writeObject(usernameNewDatabase.getText());
            out.writeObject(passwordNewDatabase.getText());

            out.writeObject(pathTextField.getText());

            out.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}

package integrador;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import objects.LearningObject;
import objects.Topico;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Parser {
    SAXBuilder builder;
    Document estruturaTopico;
    Element graph;
    String numTopico;

    public String getNumTopico() {
        return numTopico;
    }

    public void setNumTopico(String numTopico) {
        this.numTopico = numTopico;
    }

    public int getTopicoID() {
        return topicoID;
    }

    int topicoID;
    
    public Parser(int usuario, int disciplina, int topicoID) throws JDOMException, IOException {
        try {
            this.builder = new SAXBuilder();
            this.estruturaTopico = this.builder.build("D:/VertrigoServNew/www/inc/adaptweb/disciplinas/"+usuario+"/"+disciplina+"/estrutura_topico.xml");
            this.topicoID = topicoID;
        } catch (JDOMException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }

    public Parser(int usuario, int disciplina, String nomeArquivo, String numTopico) throws JDOMException, IOException {
        try {
            this.builder = new SAXBuilder();

            this.builder.setEntityResolver(new EntityResolver() {
                public InputSource resolveEntity(String publicId, String
                        systemId) throws SAXException, IOException {
                    return new InputSource("D:/VertrigoServNew/www/inc/adaptweb/dtd/Elementos_Topico.dtd");
                }
            });

            this.estruturaTopico = this.builder.build("D:/VertrigoServNew/www/inc/adaptweb/disciplinas/"+usuario+"/"+disciplina+"/"+nomeArquivo);
            this.numTopico = numTopico;
            //this.estruturaTopico = this.bu(("D:/VertrigoServNew/www/inc/adaptweb/disciplinas/"+usuario+"/"+disciplina+"/"+nomeArquivo));
        } catch (JDOMException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }
 
    public ArrayList<String> startTopicParser(int codigoUsuario, int codigoDisciplina) throws SQLException {
        Topico topico = new Topico(this.topicoID);
        ArrayList<String> queryList = new ArrayList<>();
        
        this.graph = this.estruturaTopico.getRootElement();  // Raiz.
        List<Element> document;
   
        document = graph.getChildren(); // Filhos da Raiz.
        for(Element a:document){
            queryList.addAll(topico.prepareInsertQuery(a, codigoUsuario, codigoDisciplina));
            for (Element b:a.getChildren()) {
                if (b.getName() == "topico") {
                    queryList.addAll(topico.prepareInsertQuery(b, codigoUsuario, codigoDisciplina));
                    for (Element c:b.getChildren()) {
                        if (c.getName() == "topico") {
                            queryList.addAll(topico.prepareInsertQuery(c, codigoUsuario, codigoDisciplina));
                        }
                    }
                }
            }
        }

        this.topicoID = topico.getLastID();
        return queryList;
    }
    
    public ArrayList<String> startLearningObjectParser(int codigoUsuario, int codigoTopico, int codigoDisciplina) throws SQLException {
        LearningObject learningObject = new LearningObject();
        ArrayList<String> queryList = new ArrayList<>();
        
        this.graph = this.estruturaTopico.getRootElement();  // Raiz.
        List<Element> document;
   
        document = graph.getChildren(); // Filhos da Raiz.
        
        for(Element a:document){
            queryList.addAll(learningObject.prepareInsertQuery(a, codigoUsuario, codigoDisciplina, codigoTopico, numTopico));
        } 
        
        return queryList;
    }
}

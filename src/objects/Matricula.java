package objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Matricula extends Object {
    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    int codigoUsuario;

    public int getCodigoCurso() {
        return CodigoCurso;
    }

    public void setCodigoCurso(int codigoCurso) {
        CodigoCurso = codigoCurso;
    }

    int CodigoCurso;

    @Override
    public String prepareInsertQuery(ResultSet result) throws SQLException {
        String query = new String();

        try {
            setCodigoUsuario(result.getInt(1));
            setCodigoCurso(result.getInt(2));

            query = "INSERT INTO `Matricula`(`CodigoMatricula`, `CodigoUsuario`, `CodigoCurso`) VALUES (null,'"+getCodigoUsuario() +"','"
                    +getCodigoCurso()+"')";

            return query;
        } catch (SQLException e) {
            System.out.println("Erro na obtencao de informacao do LogUsuario no banco AdaptWebClean!");
            System.out.println(e.getMessage());
        }
        return null;
    }

}

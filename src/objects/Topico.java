package objects;

import java.sql.SQLException;
import java.util.ArrayList;
import org.jdom2.Element;

public class Topico extends Object {
    private int codigoTopico;
    private int codigoUsuario;
    private int codigoCurso;
    private String numeroTopico;
    private String descricaoTopico;
    private String abreviacaoTopico;
    private String arquivoTopico;
    private String keywordTopico;
    private int statusExemploTopico;
    private int statusExercicioTopico;
    private int statusMaterialTopico;

    public int getLastID() {
        return lastID;
    }

    private int lastID;

    public Topico(int lastID) {
        this.lastID = lastID;
    }

    @Override
    public ArrayList<String> prepareInsertQuery(Element element, int codigoUsuario, int codigoDisciplina) throws SQLException {
        ArrayList<String> queryList = new ArrayList<>();
        String query = new String();
        Topico newTopic = new Topico(getLastID());

        String lastTopic = "batata";

        newTopic.setNumeroTopico(element.getAttributeValue("numtop"));
        newTopic.setDescricaoTopico(element.getAttributeValue("desctop"));
        newTopic.setAbreviacaoTopico(element.getAttributeValue("abreviacao"));
        newTopic.setArquivoTopico(element.getAttributeValue("arquivoxml"));
        newTopic.setKeywordTopico(element.getAttributeValue("palchave"));

        for (Element auxElement:element.getChildren()) {
            // Se for o mesmo topico mas tem varios cursos
            if(auxElement.getAttributeValue("identprereq") != null) {
                query = "INSERT INTO `topicorequisito`(`CodigoRequisito`, `CodigoTopico`, `NumeroTopicoRequisito`) VALUES (null," + this.lastID + ","
                        + auxElement.getAttributeValue("identprereq") + ")";
                queryList.add(query);
            }

            if (!(lastTopic.equals(newTopic.getNumeroTopico()))){
                if (auxElement.getAttributeValue("identcurso") != null) {
                    //if (auxElement.getName().equals("topico")){
                    newTopic.setCodigoCurso(Integer.valueOf(auxElement.getAttributeValue("identcurso")));
                    for (Element auxElement2 : auxElement.getChildren()) {
                        newTopic.setStatusExemploTopico(0);
                        newTopic.setStatusExercicioTopico(0);
                        newTopic.setStatusMaterialTopico(0);
                        for (Element auxElement3 : auxElement2.getChildren()) {
                            if (auxElement3.getAttribute("possuiexemp") != null) {
                                newTopic.setStatusExemploTopico(1);
                            }
                            if (auxElement3.getAttribute("possuiexerc") != null) {
                                newTopic.setStatusExercicioTopico(1);
                            }
                            if (auxElement3.getAttribute("possuimatcomp") != null) {
                                newTopic.setStatusMaterialTopico(1);
                            }
                        }

                        //System.out.println("ID: " + lastID);

                        query = "INSERT INTO `topicos`(`CodigoTopico`, `CodigoUsuario`, `CodigoDisciplina`, `NumeroTopico`, `DescricaoTopico`, "
                                + "`AbreviacaoTopico`, `ArquivoTopico`, `KeywordTopico`, `StatusExemploTopico`, `StatusExercicioTopico`, "
                                + "`StatusMaterialTopico`) VALUES (" + this.lastID + "," + codigoUsuario + "," + codigoDisciplina + ",'"
                                + newTopic.getNumeroTopico() + "','" + newTopic.getDescricaoTopico() + "','" + newTopic.getAbreviacaoTopico() + "','"
                                + newTopic.getArquivoTopico() + "','" + newTopic.getKeywordTopico() + "','" + newTopic.getStatusExemploTopico() + "','"
                                + newTopic.getStatusExercicioTopico() + "','" + newTopic.getStatusMaterialTopico() + "')";
                        lastTopic = newTopic.getNumeroTopico();
                        queryList.add(query);

                        query = "INSERT INTO `topicocurso`(`CodigoTopicoCurso`, `CodigoTopico`, `CodigoCurso`) VALUES (null," + this.lastID + ","
                                + newTopic.getCodigoCurso() + ")";
                        queryList.add(query);

                        this.lastID++;
                    }
                }
            } else if (auxElement.getAttributeValue("identcurso") != null)  {
                    int t = this.lastID-1;
                    query = "INSERT INTO `topicocurso`(`CodigoTopicoCurso`, `CodigoTopico`, `CodigoCurso`) VALUES (null," + t + ","
                            + auxElement.getAttributeValue("identcurso") + ")";
                    queryList.add(query);
            }
        }

        this.lastID = lastID;
        return queryList;
    }


    public int getCodigoTopico() {
        return codigoTopico;
    }

    public void setCodigoTopico(int codigoTopico) {
        this.codigoTopico = codigoTopico;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public int getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(int codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getNumeroTopico() {
        return numeroTopico;
    }

    public void setNumeroTopico(String numeroTopico) {
        this.numeroTopico = numeroTopico;
    }

    public String getDescricaoTopico() {
        return descricaoTopico;
    }

    public void setDescricaoTopico(String descricaoTopico) {
        this.descricaoTopico = descricaoTopico;
    }

    public String getAbreviacaoTopico() {
        return abreviacaoTopico;
    }

    public void setAbreviacaoTopico(String abreviacaoTopico) {
        this.abreviacaoTopico = abreviacaoTopico;
    }

    public String getArquivoTopico() {
        return arquivoTopico;
    }

    public void setArquivoTopico(String arquivoTopico) {
        this.arquivoTopico = arquivoTopico;
    }

    public String getKeywordTopico() {
        return keywordTopico;
    }

    public void setKeywordTopico(String keywordTopico) {
        this.keywordTopico = keywordTopico;
    }

    public int getStatusExemploTopico() {
        return statusExemploTopico;
    }

    public void setStatusExemploTopico(int statusExemploTopico) {
        this.statusExemploTopico = statusExemploTopico;
    }

    public int getStatusExercicioTopico() {
        return statusExercicioTopico;
    }

    public void setStatusExercicioTopico(int statusExercicioTopico) {
        this.statusExercicioTopico = statusExercicioTopico;
    }

    public int getStatusMaterialTopico() {
        return statusMaterialTopico;
    }

    public void setStatusMaterialTopico(int statusMaterialTopico) {
        this.statusMaterialTopico = statusMaterialTopico;
    }
}

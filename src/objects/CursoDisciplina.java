package objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CursoDisciplina  extends Object {
    private int codigoCurso;
    private int codigoDisciplina;
    private int codigoUsuario;

    @Override
    public String prepareInsertQuery(ResultSet result) throws SQLException {
        String query = new String();
        CursoDisciplina newSubject = new CursoDisciplina();

        try {
            newSubject.setCodigoCurso(result.getInt(1));
            newSubject.setCodigoUsuario(result.getInt(3));
            newSubject.setCodigoDisciplina(result.getInt(2));

            query = "INSERT INTO `CursoDisciplina`(`CodigoCurso`, `CodigoDisciplina`, `CodigoUsuario`) "
                    + "VALUES ('"+newSubject.getCodigoCurso()+"','"+newSubject.getCodigoDisciplina()+"','"+newSubject.getCodigoUsuario()+"')";

            return query;
        } catch (SQLException e) {
            System.out.println("Erro na obtencao de informacao da Disciplina no banco AdaptWebClean!");
            System.out.println(e.getMessage());
        }
        return null;
    }

    public int getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(int codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public int getCodigoDisciplina() {
        return codigoDisciplina;
    }

    public void setCodigoDisciplina(int codigoDisciplina) {
        this.codigoDisciplina = codigoDisciplina;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }
}


package objects;

import com.hp.hpl.jena.ontology.OntModel;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Disciplina extends Object {
    private int codigoDisciplina;
    private int codigoUsuario;
    private String nomeDisciplina;
    private int StatusDisciplina;
    private int StatusXMLDisciplina;

    @Override
    public String prepareInsertQuery(ResultSet result) throws SQLException {
        String query = new String();
        Disciplina newSubject = new Disciplina();
        
        try {
            newSubject.setCodigoDisciplina(result.getInt(1));
            newSubject.setCodigoUsuario(result.getInt(4));
            newSubject.setNomeDisciplina(result.getString(2));
            newSubject.setStatusDisciplina(result.getInt(5));
            newSubject.setStatusXMLDisciplina(result.getInt(6));         
            
            query = "INSERT INTO `Disciplinas`(`CodigoDisciplina`, `CodigoUsuario`, `NomeDisciplina`, "
                    + "`StatusDisciplina`, `StatusXMLDisciplina`) "
                    + "VALUES ('"+newSubject.getCodigoDisciplina()+"','"+newSubject.getCodigoUsuario()+"','"+newSubject.getNomeDisciplina()+"','"
                                 +newSubject.isStatusDisciplina()+"','"+newSubject.isStatusXMLDisciplina()+"')";
            
            return query;
        } catch (SQLException e) {
            System.out.println("Erro na obtencao de informacao da Disciplina no banco AdaptWebClean!");
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    public OntModel prepareInsertOntology(OntModel schema) {
        return schema;
    }
    
    public int getCodigoDisciplina() {
        return codigoDisciplina;
    }

    public void setCodigoDisciplina(int codigoDisciplina) {
        this.codigoDisciplina = codigoDisciplina;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getNomeDisciplina() {
        return nomeDisciplina;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
        this.nomeDisciplina = nomeDisciplina;
    }

    public int isStatusDisciplina() {
        return StatusDisciplina;
    }

    public void setStatusDisciplina(int StatusDisciplina) {
        this.StatusDisciplina = StatusDisciplina;
    }

    public int isStatusXMLDisciplina() {
        return StatusXMLDisciplina;
    }

    public void setStatusXMLDisciplina(int StatusXMLDisciplina) {
        this.StatusXMLDisciplina = StatusXMLDisciplina;
    }
    
    
}

package objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Curso extends Object {
    private int codigoCurso;
    private String nomeCurso;
    private int codigoUsuario;

    @Override
    public String prepareInsertQuery(ResultSet result) throws SQLException {
        String query = new String();
        Curso newSubject = new Curso();

        try {
            newSubject.setCodigoCurso(result.getInt(1));
            newSubject.setCodigoUsuario(result.getInt(3));
            newSubject.setNomeCurso(result.getString(2));

            query = "INSERT INTO `Curso`(`CodigoCurso`, `NomeCurso`, `CodigoUsuario`) "
                    + "VALUES ('"+newSubject.getCodigoCurso()+"','"+newSubject.getNomeCurso()+"','"+newSubject.getCodigoUsuario()+"')";

            return query;
        } catch (SQLException e) {
            System.out.println("Erro na obtencao de informacao da Disciplina no banco AdaptWebClean!");
            System.out.println(e.getMessage());
        }
        return null;
    }

    public int getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(int codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getNomeCurso() {
        return nomeCurso;
    }

    public void setNomeCurso(String nomeCurso) {
        this.nomeCurso = nomeCurso;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

}

package objects;

import org.jdom2.Element;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Keyword extends Object {
    public int getCodigoKeyword() {
        return codigoKeyword;
    }

    public void setCodigoKeyword(int codigoKeyword) {
        this.codigoKeyword = codigoKeyword;
    }

    public String getNomeKeyword() {
        return nomeKeyword;
    }

    public void setNomeKeyword(String nomeKeyword) {
        this.nomeKeyword = nomeKeyword;
    }

    private int codigoKeyword;
    private String nomeKeyword;

    @Override
    public String prepareInsertQuery(ResultSet result) throws SQLException {
        String query = new String();

        try {
            this.setNomeKeyword(result.getString(1));

            query = "INSERT INTO `Keyword`(`CodigoKeyword`, `NomeKeyword`) "
                    + "VALUES (null,'"+this.getNomeKeyword()+"')";
            return query;
        } catch (SQLException e) {
            System.out.println("Erro na obtencao de informacao da Disciplina no banco AdaptWebClean!");
            System.out.println(e.getMessage());
        }
        return null;

        //return super.prepareInsertQuery(result);
    }

}

package objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LogUsuario extends Object {
    private int codigoUsuario;
    private int codigoCurso;
    private int codigoDisciplina;
    private String numeroTopico;
    private String tipoLogUsuario;
    private String estiloLogUsuario;

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public int getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(int codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public int getCodigoDisciplina() {
        return codigoDisciplina;
    }

    public void setCodigoDisciplina(int codigoDisciplina) {
        this.codigoDisciplina = codigoDisciplina;
    }

    public String getNumeroTopico() {
        return numeroTopico;
    }

    public void setNumeroTopico(String numeroTopico) {
        this.numeroTopico = numeroTopico;
    }

    public String getTipoLogUsuario() {
        return tipoLogUsuario;
    }

    public void setTipoLogUsuario(String tipoLogUsuario) {
        this.tipoLogUsuario = tipoLogUsuario;
    }

    public String getEstiloLogUsuario() {
        return estiloLogUsuario;
    }

    public void setEstiloLogUsuario(String estiloLogUsuario) {
        this.estiloLogUsuario = estiloLogUsuario;
    }

    @Override
    public String prepareInsertQuery(ResultSet result) throws SQLException {
        String query = new String();

        try {
            setCodigoUsuario(result.getInt(2));
            setCodigoDisciplina(result.getInt(3));
            setCodigoCurso(result.getInt(4));
            setEstiloLogUsuario(result.getString(6));
            setNumeroTopico(result.getString(7));
            setTipoLogUsuario(result.getString(10));

            query = "INSERT INTO `LogUsuario`(`CodigoLogUsuario`, `CodigoUsuario`, `CodigoDisciplina`, `CodigoCurso`, `EstiloLogUsuario`, "
                    + "`NumeroTopico`, `TipoLogUsuario`) VALUES (null,'"+getCodigoUsuario()+"','"+getCodigoDisciplina()+"','"
                    +getCodigoCurso()+"','"+getEstiloLogUsuario()+"','"+getNumeroTopico()+"','"+getTipoLogUsuario()+"')";

            return query;
        } catch (SQLException e) {
            System.out.println("Erro na obtencao de informacao do LogUsuario no banco AdaptWebClean!");
            System.out.println(e.getMessage());
        }
        return null;
    }
}

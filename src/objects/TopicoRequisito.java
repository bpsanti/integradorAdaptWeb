package objects;

import objects.CursoDisciplina;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by santi on 05/06/2017.
 */
public class TopicoRequisito extends Object {
    private int codigoTopico;
    private int codigoTopicoRequisito;

    @Override
    public String prepareInsertQuery(ResultSet result) throws SQLException {
        String query = new String();
        CursoDisciplina newSubject = new CursoDisciplina();

        try {
            newSubject.setCodigoCurso(result.getInt(1));
            newSubject.setCodigoUsuario(result.getInt(3));
            newSubject.setCodigoDisciplina(result.getInt(2));

            query = "INSERT INTO `CursoDisciplina`(`CodigoCurso`, `CodigoDisciplina`, `CodigoUsuario`) "
                    + "VALUES ('"+newSubject.getCodigoCurso()+"','"+newSubject.getCodigoDisciplina()+"','"+newSubject.getCodigoUsuario()+"')";

            return query;
        } catch (SQLException e) {
            System.out.println("Erro na obtencao de informacao da Disciplina no banco AdaptWebClean!");
            System.out.println(e.getMessage());
        }
        return null;
    }

    public int getCodigoTopico() {
        return codigoTopico;
    }

    public void setCodigoTopico(int codigoTopico) {
        this.codigoTopico = codigoTopico;
    }

    public int getCodigoTopicoRequisito() {
        return codigoTopicoRequisito;
    }

    public void setCodigoTopicoRequisito(int codigoTopicoRequisito) {
        this.codigoTopicoRequisito = codigoTopicoRequisito;
    }


}

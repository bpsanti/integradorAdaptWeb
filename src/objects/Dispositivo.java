package objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class Dispositivo extends Object {
    private int codigoDispositivo;
    private int codigoUsuario;
    private Timestamp dataDispositivo;
    private String tipoDispositivo;
    private int larguraDispositivo;
    private int alturaDispositivo;
    private int latenciaDispositivo;
    private String conexaoDispositivo;

    @Override
    public String prepareInsertQuery(ResultSet result) throws SQLException {
        String query = new String();
        Dispositivo newDispositive = new Dispositivo();
        
        try {
            newDispositive.setCodigoDispositivo(result.getInt(1));
            newDispositive.setCodigoUsuario(result.getInt(2));
            newDispositive.setDataDispositivo(result.getTimestamp(3));
            newDispositive.setTipoDispositivo(result.getString(4));
            newDispositive.setLarguraDispositivo(Integer.valueOf(result.getString(5)));
            newDispositive.setAlturaDispositivo(Integer.valueOf(result.getString(6)));
            newDispositive.setLatenciaDispositivo(Integer.valueOf(result.getString(7)));
            newDispositive.setConexaoDispositivo(result.getString(8));         
            
            query = "INSERT INTO `dispositivos`(`CodigoDispositivo`, `CodigoUsuario`, `DataDispositivo`, "
                    + "`TipoDispositivo`, `LarguraDispositivo`, `AlturaDispositivo`, `LatenciaDispositivo`, `ConexaoDispositivo`) "
                    + "VALUES ('"+newDispositive.getCodigoDispositivo()+"','"+newDispositive.getCodigoUsuario()+"','"
                                 +newDispositive.getDataDispositivo()+"','"+ newDispositive.getTipoDispositivo()+"','"
                                 +newDispositive.getLarguraDispositivo()+"','"+ newDispositive.getAlturaDispositivo()+"','"
                                 +newDispositive.getLatenciaDispositivo()+"','"+newDispositive.getConexaoDispositivo()+"')";
            
            return query;
        } catch (SQLException e) {
            System.out.println("Erro na obtencao de informacao da Tecnologia no banco AdaptWebClean!");
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    public int getCodigoDispositivo() {
        return codigoDispositivo;
    }

    public void setCodigoDispositivo(int codigoDispositivo) {
        this.codigoDispositivo = codigoDispositivo;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public Timestamp getDataDispositivo() {
        return dataDispositivo;
    }

    public void setDataDispositivo(Timestamp dataDispositivo) {
        this.dataDispositivo = dataDispositivo;
    }

    public String getTipoDispositivo() {
        return tipoDispositivo;
    }

    public void setTipoDispositivo(String tipoDispositivo) {
        this.tipoDispositivo = tipoDispositivo;
    }

    public int getLarguraDispositivo() {
        return larguraDispositivo;
    }

    public void setLarguraDispositivo(int larguraDispositivo) {
        this.larguraDispositivo = larguraDispositivo;
    }

    public int getAlturaDispositivo() {
        return alturaDispositivo;
    }

    public void setAlturaDispositivo(int alturaDispositivo) {
        this.alturaDispositivo = alturaDispositivo;
    }

    public int getLatenciaDispositivo() {
        return latenciaDispositivo;
    }

    public void setLatenciaDispositivo(int latenciaDispositivo) {
        this.latenciaDispositivo = latenciaDispositivo;
    }

    public String getConexaoDispositivo() {
        return conexaoDispositivo;
    }

    public void setConexaoDispositivo(String conexaoDispositivo) {
        this.conexaoDispositivo = conexaoDispositivo;
    }
}

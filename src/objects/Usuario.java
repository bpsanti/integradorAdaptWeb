package objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Usuario extends Object {
    private int codigoUsuario;
    private String nomeUsuario;
    private String instituicaoUsuario;
    private String observacaoUsuario;
    private int statusUsuario;
    private String tipoUsuario;
    
    @Override
    public String prepareInsertQuery(ResultSet result) throws SQLException {
        String query = new String();
        Usuario newUser = new Usuario();
        
        try {
            newUser.setCodigoUsuario(result.getInt(1));
            newUser.setNomeUsuario(result.getString(2));
            newUser.setInstituicaoUsuario(result.getString(6));
            newUser.setObservacaoUsuario(result.getString(7));
            if (result.getString(10).equals("autorizado")) {
                newUser.setStatusUsuario(1);
            } else {
                newUser.setStatusUsuario(0);
            }
            newUser.setTipoUsuario(result.getString(5));         
            
            query = "INSERT INTO `usuarios`(`CodigoUsuario`, `NomeUsuario`, `InstituicaoUsuario`, "
                    + "`ObservacaoUsuario`, `StatusUsuario`, `TipoUsuario`) "
                    + "VALUES ('"+newUser.getCodigoUsuario()+"','"+newUser.getNomeUsuario()+"','"+newUser.getInstituicaoUsuario()+"','"
                                 +newUser.getObservacaoUsuario()+"','"+newUser.getStatusUsuario()+"','"+newUser.getTipoUsuario()+"')";
            
            return query;
        } catch (SQLException e) {
            System.out.println("Erro na obtencao de informacao do Usuario no banco AdaptWebClean!");
            System.out.println(e.getMessage());
        }
        return null;
    }
  
    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getInstituicaoUsuario() {
        return instituicaoUsuario;
    }

    public void setInstituicaoUsuario(String instituicaoUsuario) {
        this.instituicaoUsuario = instituicaoUsuario;
    }

    public String getObservacaoUsuario() {
        return observacaoUsuario;
    }

    public void setObservacaoUsuario(String observacaoUsuario) {
        this.observacaoUsuario = observacaoUsuario;
    }

    public int getStatusUsuario() {
        return statusUsuario;
    }

    public void setStatusUsuario(int statusUsuario) {
        this.statusUsuario = statusUsuario;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }


    
    
}

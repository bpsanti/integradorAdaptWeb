/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objects;

import java.sql.SQLException;
import java.util.ArrayList;
import org.jdom2.Element;

/**
 *
 * @author Bruno
 */
public class LearningObject extends Object {
    private int codigoLearningObject;
    private int codigoTopico;
    private int codigoCurso;
    private String nomeLearningObject;
    private String descricaoLearningObject;
    private String keywordLearningObject;
    private String arquivoLearningObject;
    private int tipoLearningObject;
    private int complexidadeLearningObject;    

//    INSERT INTO `learningobjects`(`CodigoLearningObject`, `CodigoTopico`, `CodigoCurso`, `NomeLearningObject`, `DescricaoLearningObject`, `KeywordLearningObject`, `ArquivoLearningObject`, `TipoLearningObject`, `ComplexidadeLearningObject`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9])
    @Override
    public ArrayList<String> prepareInsertQuery(Element element, int codigoUsuario, int codigoDisciplina, int codigoTopico, String numTopico) throws SQLException {
        ArrayList<String> queryList = new ArrayList<>();
        String query = new String();
        LearningObject newLearningObject = new LearningObject();
        
        //Tipo Learning Object: 0 - MatComp. 1 - Exercicio. 2 - Exemplo
        //Dificuldade 1 - fácil, 2 - medio, 3 - complexo
        switch (element.getName()) {
            case "matcomp":
                newLearningObject.setCodigoTopico(codigoTopico);
                newLearningObject.setNomeLearningObject(element.getAttributeValue("nomematcomp"));
                newLearningObject.setDescricaoLearningObject(element.getAttributeValue("descmatcomp"));
                newLearningObject.setKeywordLearningObject(element.getAttributeValue("palchavematcomp"));
                newLearningObject.setArquivoLearningObject(element.getAttributeValue("arqmatcomp"));
                newLearningObject.setTipoLearningObject(0);
                break;
            case "exercicio":
                newLearningObject.setCodigoTopico(codigoTopico);
                newLearningObject.setNomeLearningObject(element.getAttributeValue("nomeexerc"));
                newLearningObject.setDescricaoLearningObject(element.getAttributeValue("descexerc"));
                newLearningObject.setKeywordLearningObject(element.getAttributeValue("palchaveexerc"));
                newLearningObject.setArquivoLearningObject(element.getAttributeValue("arqexerc"));
                switch(element.getAttributeValue("compexerc")) {
                    case "Facil":
                        newLearningObject.setComplexidadeLearningObject(1);
                        break;
                    case "Medio":
                        newLearningObject.setComplexidadeLearningObject(2);
                        break;
                    case "Complexo":
                        newLearningObject.setComplexidadeLearningObject(3);
                        break;
                }                
                newLearningObject.setTipoLearningObject(1);
                break;
            case "exemplo":
                newLearningObject.setCodigoTopico(codigoTopico);
                newLearningObject.setNomeLearningObject(element.getAttributeValue("nomeexemp"));
                newLearningObject.setDescricaoLearningObject(element.getAttributeValue("descexemp"));
                newLearningObject.setKeywordLearningObject(element.getAttributeValue("palchaveexemp"));
                newLearningObject.setArquivoLearningObject(element.getAttributeValue("arqexemp"));
                switch(element.getAttributeValue("compexemp")) {
                    case "Fácil":
                        newLearningObject.setComplexidadeLearningObject(1);
                        break;
                    case "Medio":
                        newLearningObject.setComplexidadeLearningObject(2);
                        break;
                    case "Complexo":
                        newLearningObject.setComplexidadeLearningObject(3);
                        break;
                }   
                newLearningObject.setTipoLearningObject(2);
                break;
            default:
                return queryList;
        }

        for (Element auxElement:element.getChildren()) {
            if (auxElement.getAttributeValue("identcurso") != null) {
                newLearningObject.setCodigoCurso(Integer.valueOf(auxElement.getAttributeValue("identcurso")));

                query = "INSERT INTO `learningobjects`(`CodigoLearningObject`, `CodigoTopico`, `CodigoDisciplina`, `NomeLearningObject`, `DescricaoLearningObject`, "
                            + "`KeywordLearningObject`, `ArquivoLearningObject`, `TipoLearningObject`, `ComplexidadeLearningObject`, `NumeroTopico`) "
                            + "VALUES (null,"+newLearningObject.getCodigoTopico()+","+codigoDisciplina+",'"
                            + newLearningObject.getNomeLearningObject()+"','"+ newLearningObject.getDescricaoLearningObject()+"','"
                            + newLearningObject.getKeywordLearningObject()+"','"+newLearningObject.getArquivoLearningObject()+"',"
                            + newLearningObject.getTipoLearningObject()+","+newLearningObject.getComplexidadeLearningObject()+",'"
                            + numTopico+"')";
                queryList.add(query);
                break;
            }
        }

       
        return queryList;
    }

    public int getCodigoLearningObject() {
        return codigoLearningObject;
    }

    public void setCodigoLearningObject(int codigoLearningObject) {
        this.codigoLearningObject = codigoLearningObject;
    }

    public int getCodigoTopico() {
        return codigoTopico;
    }

    public void setCodigoTopico(int codigoTopico) {
        this.codigoTopico = codigoTopico;
    }

    public int getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(int codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getNomeLearningObject() {
        return nomeLearningObject;
    }

    public void setNomeLearningObject(String nomeLearningObject) {
        this.nomeLearningObject = nomeLearningObject;
    }

    public String getDescricaoLearningObject() {
        return descricaoLearningObject;
    }

    public void setDescricaoLearningObject(String descricaoLearningObject) {
        this.descricaoLearningObject = descricaoLearningObject;
    }

    public String getKeywordLearningObject() {
        return keywordLearningObject;
    }

    public void setKeywordLearningObject(String keywordLearningObject) {
        this.keywordLearningObject = keywordLearningObject;
    }

    public String getArquivoLearningObject() {
        return arquivoLearningObject;
    }

    public void setArquivoLearningObject(String arquivoLearningObject) {
        this.arquivoLearningObject = arquivoLearningObject;
    }

    public int getTipoLearningObject() {
        return tipoLearningObject;
    }

    public void setTipoLearningObject(int tipoLearningObject) {
        this.tipoLearningObject = tipoLearningObject;
    }

    public int getComplexidadeLearningObject() {
        return complexidadeLearningObject;
    }

    public void setComplexidadeLearningObject(int complexidadeLearningObject) {
        this.complexidadeLearningObject = complexidadeLearningObject;
    }
    
   
}
